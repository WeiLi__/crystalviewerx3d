import numpy as np

from IPython.display import HTML

# Jmol colors.  See: http://jmol.sourceforge.net/jscolors/#color_U
jmol_colors = np.array([
(1.000,0.000,0.000) ,# None
(1.000,1.000,1.000), # H
(0.851,1.000,1.000), # He
(0.800,0.502,1.000), # Li
(0.761,1.000,0.000), # Be
(1.000,0.710,0.710), # B
(0.565,0.565,0.565), # C
(0.188,0.314,0.973), # N
(1.000,0.051,0.051), # O
(0.565,0.878,0.314), # F
(0.702,0.890,0.961), # Ne
(0.671,0.361,0.949), # Na
(0.541,1.000,0.000), # Mg
(0.749,0.651,0.651), # Al
(0.941,0.784,0.627), # Si
(1.000,0.502,0.000), # P
(1.000,1.000,0.188), # S
(0.122,0.941,0.122), # Cl
(0.502,0.820,0.890), # Ar
(0.561,0.251,0.831), # K
(0.239,1.000,0.000), # Ca
(0.902,0.902,0.902), # Sc
(0.749,0.761,0.780), # Ti
(0.651,0.651,0.671), # V
(0.541,0.600,0.780), # Cr
(0.612,0.478,0.780), # Mn
(0.878,0.400,0.200), # Fe
(0.941,0.565,0.627), # Co
(0.314,0.816,0.314), # Ni
(0.784,0.502,0.200), # Cu
(0.490,0.502,0.690), # Zn
(0.761,0.561,0.561), # Ga
(0.400,0.561,0.561), # Ge
(0.741,0.502,0.890), # As
(1.000,0.631,0.000), # Se
(0.651,0.161,0.161), # Br
(0.361,0.722,0.820), # Kr
(0.439,0.180,0.690), # Rb
(0.000,1.000,0.000), # Sr
(0.580,1.000,1.000), # Y
(0.580,0.878,0.878), # Zr
(0.451,0.761,0.788), # Nb
(0.329,0.710,0.710), # Mo
(0.231,0.620,0.620), # Tc
(0.141,0.561,0.561), # Ru
(0.039,0.490,0.549), # Rh
(0.000,0.412,0.522), # Pd
(0.753,0.753,0.753), # Ag
(1.000,0.851,0.561), # Cd
(0.651,0.459,0.451), # In
(0.400,0.502,0.502), # Sn
(0.620,0.388,0.710), # Sb
(0.831,0.478,0.000), # Te
(0.580,0.000,0.580), # I
(0.259,0.620,0.690), # Xe
(0.341,0.090,0.561), # Cs
(0.000,0.788,0.000), # Ba
(0.439,0.831,1.000), # La
(1.000,1.000,0.780), # Ce
(0.851,1.000,0.780), # Pr
(0.780,1.000,0.780), # Nd
(0.639,1.000,0.780), # Pm
(0.561,1.000,0.780), # Sm
(0.380,1.000,0.780), # Eu
(0.271,1.000,0.780), # Gd
(0.188,1.000,0.780), # Tb
(0.122,1.000,0.780), # Dy
(0.000,1.000,0.612), # Ho
(0.000,0.902,0.459), # Er
(0.000,0.831,0.322), # Tm
(0.000,0.749,0.220), # Yb
(0.000,0.671,0.141), # Lu
(0.302,0.761,1.000), # Hf
(0.302,0.651,1.000), # Ta
(0.129,0.580,0.839), # W
(0.149,0.490,0.671), # Re
(0.149,0.400,0.588), # Os
(0.090,0.329,0.529), # Ir
(0.816,0.816,0.878), # Pt
(1.000,0.820,0.137), # Au
(0.722,0.722,0.816), # Hg
(0.651,0.329,0.302), # Tl
(0.341,0.349,0.380), # Pb
(0.620,0.310,0.710), # Bi
(0.671,0.361,0.000), # Po
(0.459,0.310,0.271), # At
(0.259,0.510,0.588), # Rn
(0.259,0.000,0.400), # Fr
(0.000,0.490,0.000), # Ra
(0.439,0.671,0.980), # Ac
(0.000,0.729,1.000), # Th
(0.000,0.631,1.000), # Pa
(0.000,0.561,1.000), # U
(0.000,0.502,1.000), # Np
(0.000,0.420,1.000), # Pu
(0.329,0.361,0.949), # Am
(0.471,0.361,0.890), # Cm
(0.541,0.310,0.890), # Bk
(0.631,0.212,0.831), # Cf
(0.702,0.122,0.831), # Es
(0.702,0.122,0.729), # Fm
(0.702,0.051,0.651), # Md
(0.741,0.051,0.529), # No
(0.780,0.000,0.400), # Lr
(0.800,0.000,0.349), # Rf
(0.820,0.000,0.310), # Db
(0.851,0.000,0.271), # Sg
(0.878,0.000,0.220), # Bh
(0.902,0.000,0.180), # Hs
(0.922,0.000,0.149), # Mt
])

# CPK colors in units of RGB values:
cpk_colors = np.array([ 
(1.000,0.000,0.000) ,# None
(1.000,1.000,1.000) ,# H
(1.000,0.753,0.796) ,# He
(0.698,0.133,0.133) ,# Li
(1.000,0.078,0.576) ,# Be
(0.000,1.000,0.000) ,# B
(0.784,0.784,0.784) ,# C
(0.561,0.561,1.000) ,# N
(0.941,0.000,0.000) ,# O
(0.855,0.647,0.125) ,# F
(1.000,0.078,0.576) ,# Ne
(0.000,0.000,1.000) ,# Na
(0.133,0.545,0.133) ,# Mg
(0.502,0.502,0.565) ,# Al
(0.855,0.647,0.125) ,# Si
(1.000,0.647,0.000) ,# P
(1.000,0.784,0.196) ,# S
(0.000,1.000,0.000) ,# Cl
(1.000,0.078,0.576) ,# Ar
(1.000,0.078,0.576) ,# K
(0.502,0.502,0.565) ,# Ca
(1.000,0.078,0.576) ,# Sc
(0.502,0.502,0.565) ,# Ti
(1.000,0.078,0.576) ,# V
(0.502,0.502,0.565) ,# Cr
(0.502,0.502,0.565) ,# Mn
(1.000,0.647,0.000) ,# Fe
(1.000,0.078,0.576) ,# Co
(0.647,0.165,0.165) ,# Ni
(0.647,0.165,0.165) ,# Cu
(0.647,0.165,0.165) ,# Zn
(1.000,0.078,0.576) ,# Ga
(1.000,0.078,0.576) ,# Ge
(1.000,0.078,0.576) ,# As
(1.000,0.078,0.576) ,# Se
(0.647,0.165,0.165) ,# Br
(1.000,0.078,0.576) ,# Kr
(1.000,0.078,0.576) ,# Rb
(1.000,0.078,0.576) ,# Sr
(1.000,0.078,0.576) ,# Y
(1.000,0.078,0.576) ,# Zr
(1.000,0.078,0.576) ,# Nb
(1.000,0.078,0.576) ,# Mo
(1.000,0.078,0.576) ,# Tc
(1.000,0.078,0.576) ,# Ru
(1.000,0.078,0.576) ,# Rh
(1.000,0.078,0.576) ,# Pd
(0.502,0.502,0.565) ,# Ag
(1.000,0.078,0.576) ,# Cd
(1.000,0.078,0.576) ,# In
(1.000,0.078,0.576) ,# Sn
(1.000,0.078,0.576) ,# Sb
(1.000,0.078,0.576) ,# Te
(0.627,0.125,0.941) ,# I
(1.000,0.078,0.576) ,# Xe
(1.000,0.078,0.576) ,# Cs
(1.000,0.647,0.000) ,# Ba
(1.000,0.078,0.576) ,# La
(1.000,0.078,0.576) ,# Ce
(1.000,0.078,0.576) ,# Pr
(1.000,0.078,0.576) ,# Nd
(1.000,0.078,0.576) ,# Pm
(1.000,0.078,0.576) ,# Sm
(1.000,0.078,0.576) ,# Eu
(1.000,0.078,0.576) ,# Gd
(1.000,0.078,0.576) ,# Tb
(1.000,0.078,0.576) ,# Dy
(1.000,0.078,0.576) ,# Ho
(1.000,0.078,0.576) ,# Er
(1.000,0.078,0.576) ,# Tm
(1.000,0.078,0.576) ,# Yb
(1.000,0.078,0.576) ,# Lu
(1.000,0.078,0.576) ,# Hf
(1.000,0.078,0.576) ,# Ta
(1.000,0.078,0.576) ,# W
(1.000,0.078,0.576) ,# Re
(1.000,0.078,0.576) ,# Os
(1.000,0.078,0.576) ,# Ir
(1.000,0.078,0.576) ,# Pt
(0.855,0.647,0.125) ,# Au
(1.000,0.078,0.576) ,# Hg
(1.000,0.078,0.576) ,# Tl
(1.000,0.078,0.576) ,# Pb
(1.000,0.078,0.576) ,# Bi
(1.000,0.078,0.576) ,# Po
(1.000,0.078,0.576) ,# At
(1.000,1.000,1.000) ,# Rn
(1.000,1.000,1.000) ,# Fr
(1.000,1.000,1.000) ,# Ra
(1.000,1.000,1.000) ,# Ac
(1.000,0.078,0.576) ,# Th
(1.000,1.000,1.000) ,# Pa
(1.000,0.078,0.576) ,# U
(1.000,1.000,1.000) ,# Np
(1.000,1.000,1.000) ,# Pu
(1.000,1.000,1.000) ,# Am
(1.000,1.000,1.000) ,# Cm
(1.000,1.000,1.000) ,# Bk
(1.000,1.000,1.000) ,# Cf
(1.000,1.000,1.000) ,# Es
(1.000,1.000,1.000) ,# Fm
(1.000,1.000,1.000) ,# Md
(1.000,1.000,1.000) ,# No
(1.000,1.000,1.000)  # Lw
])

def align_2v_3d(ai,bi):
    """
    align 2 3d vecter ai to bi

    return rotation the matrix

    https://math.stackexchange.com/a/476311
    """
    a, b =  ai / np.linalg.norm(ai), bi / np.linalg.norm(bi)
    v = np.cross(a,b)
    c = a.dot(b)
    v1,v2,v3 = v
    v_x = np.array([[  0.,-v3, v2],
                    [ v3,  0.,-v1],
                    [-v2,  v1, 0.]])

    return np.eye(3) + v_x + v_x.dot(v_x)/(1+c)

def align_2v_3d_x3drot(si,di):
    """
    rot a vector si to di around a axis(x,y,z) with an angle(theta)

    return [x,y,z,theta]
    """
    s = np.array(si)
    s = s/np.linalg.norm(s)
    d = np.array(di)
    d = d/np.linalg.norm(d)
    theta = np.arccos(s.dot(d))
    if np.isclose(theta,np.pi) or np.isclose(theta,0.0):
        rotaxis = d
        theta = 0.0
    else:
        rotaxis = np.cross(s,d)
    return rotaxis.tolist()+[theta]

def R2AxisAngle(R):
    
    """
    from a Rotation Matrix(R) to get the roation axis(x,y,z) and angle(theta)

    return [x,y,z,theta]

    http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
    
    Q.w = sqrt( max( 0, 1 + m00 + m11 + m22 ) ) / 2;
    Q.x = sqrt( max( 0, 1 + m00 - m11 - m22 ) ) / 2;
    Q.y = sqrt( max( 0, 1 - m00 + m11 - m22 ) ) / 2;
    Q.z = sqrt( max( 0, 1 - m00 - m11 + m22 ) ) / 2;
    Q.x = _copysign( Q.x, m21 - m12 )
    Q.y = _copysign( Q.y, m02 - m20 )
    Q.z = _copysign( Q.z, m10 - m01 )
    """
    R00,R01,R02,R10,R11,R12,R20,R21,R22 = R.flatten()
    w = 0.5*np.sqrt(np.max((0.0,1.0 + R00+R11+R22)))
    x = 0.5*np.sqrt(np.max((0.0,1.0 + R00-R11-R22)))
    y = 0.5*np.sqrt(np.max((0.0,1.0 - R00+R11-R22)))
    z = 0.5*np.sqrt(np.max((0.0,1.0 - R00-R11+R22)))
    x = np.copysign(x,R21-R12)
    y = np.copysign(y,R02-R20)
    z = np.copysign(z,R10-R01)
    
#     return [x,y,z,w]

    sin = np.sqrt(1 - w*w)
    theta = 2*np.arccos(w)
    u = x/sin
    v = y/sin
    w = z/sin
    
    return [u,v,w,theta]

M2R = R2AxisAngle

def AxisAngle2R(axisangle):
    """
    AxisAngle to Matrix
    
    http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm
    
    """
    
    x,y,z,theta = axisangle
    r = np.sqrt(x*x + y*y + z*z)
    x /=r
    y /=r
    z /=r
    
    c = np.cos(theta)
    s = np.sin(theta)
    t = 1 - c
    
    R = [[t*x*x + c,  t*x*y - z*s,t*x*z + y*s],
         [t*x*y + z*s,t*y*y + c,  t*y*z - x*s],
         [t*x*z - y*s,t*y*z + x*s,t*z*z + c  ]]
    
    return np.array(R)

def list2str(a):
    if type(a) in [bool,str,float,int]:
        return str(a)
    b = np.array(a)
    if len(b.shape) == 2:
        return ", ".join( map(lambda x: str(x)[1:-1], b))
    if len(b.shape) == 1:
        return str(b)[1:-1]

def html_(self,l,indent=0,indent0=0):

    dic = self.copy()
    child = []
    isScene = False
    if "Scene" in dic:
        scene = dic['Scene']
        dic.pop('Scene')
        child.extend(scene)
        isScene =True
        
    p = [ " {:s}=\"{:s}\"".format(i,list2str(dic[i])) for i in dic if not isinstance(dic[i], dict)]
    child.extend([ {i:dic[i]} for i in dic if isinstance(dic[i], dict) ])
    
    if indent != indent0:
        # l += " ".join(p) + "> \n"
        l += " ".join(p) + ">"
        if len(child) >0:
            l +=" \n"

    if isScene:
        indent += 4
        l += ' '*indent + '<Scene>\n'
        
    if len(child) > 0:
        for i in child:
            indent += 4
            l += ' '*indent + '<' + i.keys()[0]
            l, indent = html(i.values()[0],l,indent)
            l += ' '*indent + '</' + i.keys()[0] + '> \n'
            indent -= 4

    if isScene:
        l += ' '*indent + '</Scene>\n'
        indent -= 4

    return l, indent

def properNchild(dic):

    properties = []
    children = []
    
    for k,v in dic.items():
        if k == "children":
            if type(v) is list:
                children += v
            elif type(v) is dict:
                children += [v]
        elif type(v) is dict:
            children += [{k:v}]
        else:
            properties += [ '{:s}="{:s}"'.format(k,list2str(v))]

    return properties,children

def html(x3dic,l="",indent=-4):
        
    name = list(x3dic.keys())[0]
    
    if "Scene" in x3dic[name]:
        Scene = x3dic[name]["Scene"]
        if type(Scene) is list:
            x3dic[name]["Scene"] = {"children":Scene}
    
    properties, children = properNchild(x3dic[name])

    indent += 4
    l += " "* indent
    
    l += '<' + name + " "*int(bool(len(properties))) + " ".join(properties) + '>'
    
    if len(children) > 0:
        l += '\n'
        for child in children:
            l, indent = html(child,l,indent)            
        l += " "*indent
        
    l += '</' + name + '>\n'    
    indent -= 4
    
    return l,indent

class Group(dict):

    def __init__(self, children):
        a = {'Group': 
                {'bboxCenter': [0, 0, 0],
                 'bboxSize': [-1, -1, -1],
                 'children': children,
                 'metadata': 'X3DMetadataObject',
                 'render': True
                 }
             } 
        super(Group, self).__init__(**a)

    @property
    def children(self):
        return self["Group"]["children"]

    @children.setter
    def children(self,value):
        self["Group"]["children"]=value

class Transform(dict):
    def __init__(self, children):
        a = {'Transform':
                { 'translation': [0,0,0],
                  'rotation':[0,0,1,0],
                  'children': children
                }
            }
        super(Transform, self).__init__(**a)

    def update(self,value):
        self["Transform"].update(value)

    @property
    def children(self):
        return self["Transform"]["children"]

    @children.setter
    def children(self,value):
        self["Transform"]["children"]=value

    @property
    def translation(self):
        return self['Transform']['translation']
    
    @translation.setter
    def translation(self,value):
        self['Transform']['translation'] = value
        
    @property
    def rotation(self):
        return self['Transform']['rotation']
    
    @rotation.setter
    def rotation(self,value):
        self['Transform']['rotation'] = value

class GeometryProperty(dict):

    def __init__(self):
        a = {'Transform': 
                {'Shape': 
                    {'Appearance': 
                        {'Material': 
                            {'diffuseColor': [255,0,0],
                                'specularColor': [0.5, 0.5, 0.5],
                                'transparency': [0.0] }
                            } 
                    },
                'translation': [0, 0, 0],
                'rotation':[0,0,1,0] }
             }
        
        super(GeometryProperty, self).__init__(**a)
    
    @property
    def translation(self):
        return self['Transform']['translation']
    
    @translation.setter
    def translation(self,value):
        self['Transform']['translation'] = value
        
    @property
    def rotation(self):
        return self['Transform']['rotation']
    
    @rotation.setter
    def rotation(self,value):
        self['Transform']['rotation'] = value

    @property
    def diffuseColor(self):
        return self['Transform']['Shape']['Appearance']['Material']['diffuseColor']
    
    @diffuseColor.setter
    def diffuseColor(self,value):
        self['Transform']['Shape']['Appearance']['Material']['diffuseColor'] = value

    @property
    def specularColor(self):
        return self['Transform']['Shape']['Appearance']['Material']['specularColor']
    
    @specularColor.setter
    def specularColor(self,value):
        self['Transform']['Shape']['Appearance']['Material']['specularColor'] = value

    @property
    def transparency(self):
        return self['Transform']['Shape']['Appearance']['Material']['transparency']
        
    @transparency.setter
    def transparency(self,value):
        self['Transform']['Shape']['Appearance']['Material']['transparency'] = value
        
    
class Sphere(GeometryProperty):
    
    def __init__(self, radius=1, position=[0,0,0], color=[1,0,0],transparency=0.0):
        super(Sphere, self).__init__()
        self['Transform']['Shape'].update({'Sphere': {'radius': [radius]}})
        self.translation = position
        self.diffuseColor = color
        self.transparency = [transparency]
    
    @property
    def radius(self):
        return self['Transform']['Shape']['Sphere']['radius']
    
    @radius.setter
    def radius(self,value):
        self['Transform']['Shape']['Sphere']['radius'] = value


Shere = Sphere


class Box(GeometryProperty):
    def __init__(self, size=[2,2,2],color=[1,0,0]):
        a ={'Box': {'ccw': True,
                    'hasHelperColors': False,
                    'lit': True,
                    'metadata': 'X3DMetadataObject',
                    'size': size,
                    'solid': True,
                    'useGeoCache': True}}

        super(Box, self).__init__()
        self['Transform']['Shape'].update(a)
        self.diffuseColor = color

    @property
    def color(self):
        return self.diffuseColor
    @color.setter
    def color(self,value):
        self.diffuseColor = value

class Cone(GeometryProperty):
    def __init__(self, direction=[0,0,1], scale=1.0, bottomRadius=1.0,height=2.0, topRadius=0, color=[1,0,0]):
        a = {'Cone': {'bottom': True,
                      'bottomRadius': [bottomRadius*scale],
                      'ccw': True,
                      'height': [height*scale],
                      'lit': True,
                      'metadata': 'X3DMetadataObject',
                      'side': True,
                      'solid': True,
                      'subdivision': [32],
                      'top': True,
                      'topRadius': [topRadius*scale],
                      'useGeoCache': True}}

        super(Cone, self).__init__()
        self['Transform']['Shape'].update(a)
        self.diffuseColor = color
        self.rotation = R2AxisAngle(align_2v_3d([0,1,0],direction))
        self.translation = direction

    @property
    def color(self):
        return self.diffuseColor
    @color.setter
    def color(self,value):
        self.diffuseColor = value

    

class IndexedLineSet(GeometryProperty):
    def __init__(self, points=[], coordIndex=[], colors=[], colorIndex=[]):
        a = { 'IndexedLineSet':
                 { 'coordIndex': coordIndex,
                   'colorIndex': colorIndex,
                   'Coordinate': {'point':points} ,
                   'Color'     : {'color':colors}
                 }                        
            }
        super(IndexedLineSet, self).__init__()
        self['Transform']['Shape'].update(a)
    
    @property
    def coordIndex(self):
        return self['Transform']['Shape']['IndexedLineSet']['coordIndex']
    @coordIndex.setter
    def coordIndex(self,value):
        self['Transform']['Shape']['IndexedLineSet']['coordIndex'] = value
        
    @property
    def colorIndex(self):
        return self['Transform']['Shape']['IndexedLineSet']['colorIndex']
    
    @colorIndex.setter
    def colorIndex(self,value):
        self['Transform']['Shape']['IndexedLineSet']['colorIndex'] = value
        
    @property
    def point(self):
        return self['Transform']['Shape']['IndexedLineSet']['Coordinate']['point']
    
    @point.setter
    def point(self,value):
        self['Transform']['Shape']['IndexedLineSet']['Coordinate']['point'] = value
        
    @property
    def color(self):
        return self['Transform']['Shape']['IndexedLineSet']['Color']['color']
    
    @color.setter
    def color(self,value):
        self['Transform']['Shape']['IndexedLineSet']['Color']['color'] = value

class Plane(GeometryProperty):
    """<Plane ccw='true' center='0,0,0' lit='true' metadata='X3DMetadataObject' primType='['TRIANGLES']' size='2,2' solid='true' subdivision='1,1' useGeoCache='true' ></Plane>"""
    def __init__(self, center=[0,0,0], size=[2,2], subdivision=[1,1], solid=True):
        a = { 'Plane':
                 { 'ccw' : 'true',
                   'center'      : center,
                   'lit'         : 'true',
                   'metadata'    : 'X3DMetadataObject',
                   'primType'    : '[TRIANGLES]',
                   'size'        : size,
                   'solid'       : solid,
                   'subdivision' : subdivision,
                   'useGeoCache' : 'true'
                 }                        
            }
        super(Plane, self).__init__()
        self['Transform']['Shape'].update(a)

    @property
    def size(self):
        return self['Transform']['Shape']['Plane']['size']
    
    @size.setter
    def size(self,value):
        self['Transform']['Shape']['Plane']['size']=value

    @property
    def center(self):
        return self['Transform']['Shape']['Plane']['center']
    
    @center.setter
    def center(self,value):
        self['Transform']['Shape']['Plane']['center']=value

    @property
    def solid(self):
        return self['Transform']['Shape']['Plane']['solid']
    
    @solid.setter
    def solid(self,value):
        self['Transform']['Shape']['Plane']['solid']=value

    @property
    def subdivision(self):
        return self['Transform']['Shape']['Plane']['subdivision']
    
    @subdivision.setter
    def subdivision(self,value):
        self['Transform']['Shape']['Plane']['subdivision']=value


class ParticleSet(GeometryProperty):
    """
    mode: [ViewDirQuads, Points, Lines, Arrows, ViewerArrows, ViewerQuads, Rectangles]
    """
    def __init__(self, points=[], size=[], colors=[],mode='Points'):
        a = { 'ParticleSet':
                 { 'drawOrder' : 'Any',
                   'size'      : size,
                   'mode'      : mode,
                   'Coordinate': {'point':points} ,
                   'Color'     : {'color':colors}
                 }                        
            }
        super(ParticleSet, self).__init__()
        self['Transform']['Shape'].update(a)

    @property
    def point(self):
        return self['Transform']['Shape']['ParticleSet']['Coordinate']['point']
    
    @point.setter
    def point(self,value):
        self['Transform']['Shape']['ParticleSet']['Coordinate']['point'] = value
        
    @property
    def color(self):
        return self['Transform']['Shape']['ParticleSet']['Color']['color']
    
    @color.setter
    def color(self,value):
        self['Transform']['Shape']['ParticleSet']['Color']['color'] = value
    
    @property
    def size(self):
        return self['Transform']['Shape']['ParticleSet']['size']
    
    @size.setter
    def size(self,value):
        self['Transform']['Shape']['ParticleSet']['size'] = value

class IndexedFaceSet(GeometryProperty):
    """
        <IndexedFaceSet solid='false' coordIndex='0 1 2 -1'>
              <Coordinate point='1 0 0 0 2 0 0 0 3'/>
              <Color color='1 0 0 0 1 0 0 0 1'/>
        </IndexedFaceSet>  
    """
    def __init__(self, points=[], coordIndex=[], colors=[], colorIndex=[]):
        a = { 'IndexedFaceSet':
                 { 
                   'solid'     : 'false',
                   'coordIndex': coordIndex,
                   'colorIndex': colorIndex,
                   'Coordinate': {'point':points}, 
                   'Color'     : {'color':colors}


                 }                        
            }
        super(IndexedFaceSet, self).__init__()
        self['Transform']['Shape'].update(a)

    @property
    def point(self):
        return self['Transform']['Shape']['IndexedFaceSet']['Coordinate']['point']
    
    @point.setter
    def point(self,value):
        self['Transform']['Shape']['IndexedFaceSet']['Coordinate']['point'] = value
        
    @property
    def color(self):
        return self['Transform']['Shape']['IndexedFaceSet']['Color']['color']
    
    @color.setter
    def color(self,value):
        self['Transform']['Shape']['IndexedFaceSet']['Color']['color'] = value

def Line(s,e,c):
    return IndexedLineSet([s,e],[0,1,-1],[c],[0,0,-1])
    

class TextObj(GeometryProperty):
    """
    <text string="&quot;Times 32px&quot; &quot;begin; default quality&quot;" solid="false" ccw="true" usegeocache="true" lit="true">
        <fontstyle family="'Times' 'Orbitron'" size="0.8" justify="begin" horizontal="true" lefttoright="true" spacing="1" style="PLAIN" toptobottom="true" quality="2"></fontstyle>
    </text>
    """

    def __init__(self,text="A",position=[0,0,0],size=0.8,color=[1,1,1]):
        a = { 'text':
                {
                    'string': text,
                    'solid':"false",
                    'ccw':"true",
                    'usegeocache':"true",
                    'lit':"true",
                    'fontstyle': {
                        'family':"'Times' 'Orbitron'",
                        'size': [size],
                        'justify':"begin",
                        'horizontal': "true",
                        'lefttoright': "true",
                        'spacing':'1',
                        'style':"PLAIN",
                        'toptobottom':"true",
                        'quality':[2],
                        }
                    }

                }

        super(TextObj, self).__init__()
        self['Transform']['Shape'].update(a)
        self.translation = position
        self.specularColor = color
        self.diffuseColor = [1,1,1]

class Scene(dict):

    def __init__(self, scene = [], width=800, height=600, OrthoViewpoint = True, orientation=[0,0,0,1], position=[0,0,10],fieldOfView=0.8, quality='high', skyColor=[0.0,0.0,0.0], CoR=[0,0,0], jslocal=True):
        '''
        X3dom all html need
        '''
        #https://github.com/x3dom/x3dom/issues/484
        if OrthoViewpoint:
            OVP =  [{'OrthoViewpoint':{
                                       'fieldOfView': np.array([-1, -1, 1 ,1])*fieldOfView*7,
                                       'centerOfRotation':CoR,
                                       'orientation': orientation,
                                       'position': position
                                       }}]
            bkn = 1
        else:
            OVP = [{'Viewpoint':{
                                 'fieldOfView': [fieldOfView],
                                 'centerOfRotation':CoR,
                                 'orientation': orientation,
                                 'position': position
                }}]
            bkn = 1

        SSAO = [{ 'environment': {
                                    'ssao':'true',
                                    'ssaoamount': [0.3],
                                    'ssaoblurdepthtreshold':[1.0],
                                    'ssaoradius':[0.7]
                                }
                }]

        SSAO = []

        a = { 'html': 
                {'body': 
                     {'X3D': 
                          {#'style' :'margin:0; padding:0; width:100%; height:100%; border:none; background: rgba(0, 0, 0, 1.0);',
                           'style' :'margin:0; padding:0; width:100%; height:100%; border:none;',
                           'Scene' : [{'Background':{'skyColor': skyColor}}, 
                                      ]  + SSAO + OVP + scene,
                           'PrimitiveQuality':quality,
                           'width' : '{}px'.format(width),
                           'height': '{}px'.format(height),
                          }
                     },
                 'head': 
                     {  'link': 
                          {
                           #'href': 'https://www.x3dom.org/x3dom/release/x3dom.css',
                            'href': 'x3dom.css',
                            'rel': 'stylesheet',
                           'type': 'text/css'
                          },
                      'script': 
                            {
                             #'src': 'https://www.x3dom.org/x3dom/release/x3dom.js',
                              'src': 'x3dom.js',
                             'type': 'text/javascript'
                            }
                     }
                }
            }
        super(Scene, self).__init__(**a)
        self.js = ['https://www.x3dom.org/x3dom/release/','x3dom.css','x3dom.js']

        if not jslocal:
            self['html']['head']['link']['href'] = self.js[0]+self.js[1]
            self['html']['head']['script']['src'] = self.js[0]+self.js[2]
        else:
            self['html']['head']['link']['href'] = self.js[1]
            self['html']['head']['script']['src'] =self.js[2]
        
    @property
    def scene(self):
        return self['html']['body']['X3D']['Scene']
    
    @scene.setter
    def scene(self,value):
        bk = self['html']['body']['X3D']['Scene'][0:bkn]
        self['html']['body']['X3D']['Scene'] = [bk] + value

    @property
    def jslocal(self):
        if 'www' in self['html']['head']['link']['href']:
            return False 
        else:
            return True

    @jslocal.setter
    def jslocal(self,value):
        if not value:
            self['html']['head']['link']['href'] = self.js[0]+self.js[1]
            self['html']['head']['script']['src'] = self.js[0]+self.js[2]
        else:
            self['html']['head']['link']['href'] = self.js[1]
            self['html']['head']['script']['src'] =self.js[2]
        
    def view(self):
        l = ''
        a, b = html(self,l)
        return HTML(a)
    
    def _html(self):
        l = ''
        a, b = html(self,l)
        return a

def viewer(structure, sc= [1,1,1], tau=False, ball=True, ps=1.0, transparency=0.0, debug=False, object3d=[], showxyz=True, bgw=False, shift=np.zeros(3) , *args, **kwargs):

    '''
    parameters:
        structure:  pymatgen structure.
        sc       :  super cell [nx,ny,nz] to display.
        tau      :  
               =Ture  : only show the atom in the super cell.
               =False : also show the atom on all edges.
        ball   =True  : use sheres 
               =False : use ParticleSet

        ps       :   (float) particle sizes scale
        debug    :
               =True  : return the scene
               =True  : return the scene.view()

        args and kwargs: parameters for Scene.
               width=800                    : 
               height=600                   :
               OrthoViewpoint = False       :  Perspective (False) or Ortho View (True).
               quality='high'               :  "Low", "high","Medium" 
               skyColor=[0.0,0.0,0.0]       :  background color.
               CoR=[0,0,0]                  :  center of the rotation.
    '''
    sc = np.array(sc)
    scp1 = sc
    
    if tau == False:
        scp1 = sc + 1

    strp1 = structure.copy() 

    strp1.make_supercell(scp1)

    scm = structure.lattice.matrix*sc.reshape(3,1)
    
    [ a,b,c], o = scm, np.zeros(3)

    vertex = [o,  o,   a,   a+b,   b,
                o+c, a+c, a+b+c, b+c,
                  o,   c,     a, a+c,   b, b+c, a+b, a+b+c]

    index = [1,2,3,4,1,-1,
             5,6,7,8,5,-1,
             9,10,-1,
             11,12,-1,
             13,14,-1,
             15,16,-1]

    if showxyz:
        index+= [1,2,-1,
                 1,4,-1,
                 1,10,-1]

    [r,g,b],w,d = np.eye(3),np.ones(3), np.zeros(3)

    
    framcolor=4 if bgw else 3

    colorindex = [framcolor]*24 

    if showxyz:
        colorindex+= [0,0,-1,
                      1,1,-1,
                      2,2,-1]

    ILS = IndexedLineSet(vertex,index,[r,g,b,w,d],colorindex)

    ILS.translation = shift

    a = strp1.frac_coords.copy()
    eln = np.array([ i.number for i in strp1.species])
    
    m = np.abs(a[:,0] - 1.0) < 1e-5
    a[m] -= np.eye(3)[0]
    
    m = np.abs(a[:,1] - 1.0) < 1e-5
    a[m] -= np.eye(3)[1]
    
    m = np.abs(a[:,2] - 1.0) < 1e-5
    a[m] -= np.eye(3)[2]
    
    ac = a.dot(strp1.lattice.matrix) + shift
    a *= scp1
    
    marsk = ( (a[:,0] <= sc[0]+1e-2) & (a[:,1] <= sc[1]+1e-2) & (a[:,2] <= sc[2]+1e-2 ))

    if ball:
        atoms = [ Shere(0.2*ps,i,jmol_colors[j],transparency) for i,j in zip(*[ac[marsk],eln[marsk]])]
    else:
        atoms = ParticleSet( ac[marsk], [[0.02*ps]*3]*len(ac[marsk]), [ jmol_colors[i] for i in eln[marsk]])
        atoms.specularColor = ""
        atoms.diffuseColor = ""
        atoms = [ atoms ]
    
    CoR = scm.sum(axis=0)/ float(len(scm))

    skyColor = np.ones(3) if bgw else np.zeros(3)
    
    crystal = Scene( scene=[ ILS ] + atoms + object3d , CoR=CoR, skyColor=skyColor, *args, **kwargs)

    if debug:
        return crystal, [ILS] + atoms, CoR

    else:
        return crystal.view()


def viewer_2(str1,str2,sc1=[1,1,1],sc2=[1,1,1],sft1=np.zeros(3),sft2=np.zeros(3),tsp1=0.0,tsp2=0.5,ps1=1,ps2=1,ball1=True,ball2=True,obj3d=[],debug=False,*args,**kwargs):

    a, s1, c1 = viewer(str1,debug=True,sc=sc1,transparency=tsp1,ball=ball1,ps=ps1,shift=sft1,*args,**kwargs)
    
    a, s2, c2 = viewer(str2,debug=True,sc=sc2,transparency=tsp2,ball=ball2,ps=ps2,shift=sft2,*args,**kwargs)

    CoR = (c1 + c2)*0.5
    
    crystal = Scene( scene=s1+s2+obj3d , CoR=CoR, *args, **kwargs)
    
    if debug:
        return crystal
    else:
        return crystal.view()
